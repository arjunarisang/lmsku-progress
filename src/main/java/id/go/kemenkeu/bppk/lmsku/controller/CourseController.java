package id.go.kemenkeu.bppk.lmsku.controller;

import id.go.kemenkeu.bppk.lmsku.entity.Course;
import id.go.kemenkeu.bppk.lmsku.enums.CourseType;
import id.go.kemenkeu.bppk.lmsku.service.CourseService;
import id.go.kemenkeu.bppk.lmsku.util.BasicResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping
    public Page<Course> findAll(Pageable pageable) {
        return courseService.findAll(pageable);
    }

    @GetMapping("/type/{courseType}")
    public Page<Course> findAllByCourseType(@PathVariable("courseType") CourseType type,
                                            Pageable pageable) {
        return courseService.findAllByCourseType(type, pageable);
    }

    @GetMapping("/{id}")
    public Course getById(@PathVariable("id") String id) {
        return courseService.getById(id);
    }

    @PostMapping
    public BasicResponse create(@Valid @RequestBody Course course) {
        courseService.create(course);

        return new BasicResponse("Course berhasil disimpan");
    }

    @PatchMapping("/{id}")
    public Course update(@PathVariable("id") String id,
                         @RequestBody Course newz) {
        Course old = courseService.getById(id);

        return courseService.update(newz, old);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        Course old = courseService.getById(id);

        courseService.delete(old);
    }
}
