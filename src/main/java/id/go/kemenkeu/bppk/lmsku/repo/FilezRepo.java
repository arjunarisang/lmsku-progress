package id.go.kemenkeu.bppk.lmsku.repo;

import id.go.kemenkeu.bppk.lmsku.entity.Filez;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilezRepo extends JpaRepository<Filez, String> {
}
