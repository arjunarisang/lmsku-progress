package id.go.kemenkeu.bppk.lmsku.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum CourseType {

    CLASSICAL("CLASSICAL"),
    E_LEARNING("E_LEARNING"),
    MICROLEARNING("MICROLEARNING"),
    PJJ("PELATIHAN JARAK JAUH");

    @JsonValue
    String courseType;

    CourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCourseType() {
        return courseType;
    }
}
