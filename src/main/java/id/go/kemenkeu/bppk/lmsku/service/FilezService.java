package id.go.kemenkeu.bppk.lmsku.service;

import id.go.kemenkeu.bppk.lmsku.entity.Filez;
import id.go.kemenkeu.bppk.lmsku.exception.EntityNotFoundException;
import id.go.kemenkeu.bppk.lmsku.repo.FilezRepo;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import io.minio.http.Method;
import lombok.RequiredArgsConstructor;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
@RequiredArgsConstructor
public class FilezService {

    private final MinioClient minioClient;
    private final FilezRepo filezRepo;

    public Filez upload(MultipartFile file) throws Exception {
        Tika tika = new Tika();
        String mime = tika.detect(file.getOriginalFilename());
        String bucketName = "belajar-java";
        // unggul diganti ->  nama masing-masing
        String objectName = "unggul/" + file.getOriginalFilename();
        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucketName)
                        .object(objectName)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType(mime)
                        .build());
        Filez filez =
                Filez
                        .builder()
                        .name(file.getOriginalFilename())
                        .bucket(bucketName)
                        .object(objectName)
                        .groupUid(UUID.randomUUID().toString())
                        .isOriginal(true)
                        .contentType(mime)
                        .build();
        return filezRepo.save(filez);
    }

    public String getPresignedUrl(String id) throws Exception {
        Filez filez = filezRepo.findById(id).orElseThrow(EntityNotFoundException::new);

        return minioClient.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs
                        .builder()
                        .method(Method.GET)
                        .bucket(filez.getBucket())
                        .object(filez.getObject())
                        .expiry(5, TimeUnit.DAYS)
                        .build()
        );
    }
}
