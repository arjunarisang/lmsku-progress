package id.go.kemenkeu.bppk.lmsku.service;

import com.github.slugify.Slugify;
import id.go.kemenkeu.bppk.lmsku.entity.Course;
import id.go.kemenkeu.bppk.lmsku.enums.CourseType;
import id.go.kemenkeu.bppk.lmsku.exception.BadRequestException;
import id.go.kemenkeu.bppk.lmsku.exception.EntityNotFoundException;
import id.go.kemenkeu.bppk.lmsku.repo.CourseRepo;
import id.go.kemenkeu.bppk.lmsku.util.IgnoreProps;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class CourseService {

    private final CourseRepo courseRepo;

    public CourseService(CourseRepo courseRepo) {
        this.courseRepo = courseRepo;
    }

    public Page<Course> findAll(Pageable pageable) {
        return courseRepo.findAll(pageable);
    }

    public List<Course> findAll() {
        return courseRepo.findAll();
    }

    public Optional<Course> findById(String id) {
        return courseRepo.findById(id);
    }

    public Course getById(String id) {
        return courseRepo.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    public Course create(Course course) {
        if (course.getStartDate().isAfter(course.getEndDate())) {
            throw new BadRequestException("Tanggal tidak valid");
        }
        /*if (LocalDateTime.now().isAfter(course.getEndDate()) || LocalDateTime.now().isAfter(course.getStartDate())) {
            throw new BadRequestException("Tanggal tidak valid");
        }*/

        Slugify slugify = new Slugify();
        String uuid = UUID.randomUUID().toString().substring(0, 7);
        String slug = slugify.slugify(course.getName()) + "-" + uuid;
        course.setSlug(slug);

        return courseRepo.save(course);
    }

    public Course update(Course newz, Course old) {
        String[] propsToIgnore =
                IgnoreProps.addPropsToIgnore(
                        IgnoreProps.invoke(newz),
                        "slug"
                );
        String oldName = old.getName();
        String newName = newz.getName();

        //BeanUtils.copyProperties(newz, old, "id", "createdAt", "createdBy", "updatedAt", "updatedBy", "slug");
        BeanUtils.copyProperties(newz, old, propsToIgnore);

        if (newName != null && !oldName.equalsIgnoreCase(newName)) {
            Slugify slugify = new Slugify();
            String uuid = UUID.randomUUID().toString().substring(0, 7);
            String slug = slugify.slugify(newz.getName()) + "-" + uuid;

            newz.setSlug(slug);
        }

        return courseRepo.save(old);
    }

    public void delete(Course course) {
        courseRepo.delete(course);
    }

    public Page<Course> findAllByCourseType(CourseType type, Pageable pageable) {
        return courseRepo.findAllByCourseType(type, pageable);
    }

}
