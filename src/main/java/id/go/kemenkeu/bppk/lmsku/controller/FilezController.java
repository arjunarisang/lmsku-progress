package id.go.kemenkeu.bppk.lmsku.controller;

import id.go.kemenkeu.bppk.lmsku.entity.Filez;
import id.go.kemenkeu.bppk.lmsku.service.FilezService;
import id.go.kemenkeu.bppk.lmsku.util.BasicResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FilezController {

    private final FilezService filezService;

    @PostMapping("/upload")
    public Filez upload(@RequestParam(value = "file") MultipartFile file) throws Exception {
        return filezService.upload(file);
    }

    @GetMapping("/{id}")
    public BasicResponse getUrlByFileId(@PathVariable("id") String id) throws Exception {
        return new BasicResponse(filezService.getPresignedUrl(id));
    }

}
