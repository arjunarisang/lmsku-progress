package id.go.kemenkeu.bppk.lmsku.repo;

import id.go.kemenkeu.bppk.lmsku.entity.Course;
import id.go.kemenkeu.bppk.lmsku.enums.CourseType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepo extends JpaRepository<Course, String> {

    Page<Course> findAllByCourseType(CourseType type, Pageable pageable);

}
