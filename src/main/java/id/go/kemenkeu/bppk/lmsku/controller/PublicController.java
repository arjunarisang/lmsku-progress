package id.go.kemenkeu.bppk.lmsku.controller;

import id.go.kemenkeu.bppk.lmsku.entity.Course;
import id.go.kemenkeu.bppk.lmsku.repo.CourseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Profile(value = {"!standalone"})
@RestController
@RequestMapping("/public")
public class PublicController {

    @Autowired
    private CourseRepo courseRepo;

    @Transactional
    public void joinCourse() {
        //begin transaction

        //batch-nya masih open gak?
        //classroom-nya masih ada kuota gak?
        //coursenya KEMENKEU/PUBLIC?

        //classroom.maxPartipant -> kurangi satu
        //gangguan DB
        //course.joinedParticipant -> tambahi satu

        //baru joinkan orang ini ke course
        //tambahkan courseHistory untuk ybs

        //commit ke db
    }

    @PostMapping("/course")
    public Course createCourse(@RequestBody Course course) {
        return courseRepo.save(course);
    }

    @GetMapping("/course")
    public List<Course> findAllCourse() {
        return courseRepo.findAll();
    }

    @GetMapping("/course/paged")
    public Page<Course> findAllCoursePaged(Pageable pageable) {
        return courseRepo.findAll(pageable);
    }

    @GetMapping("/course_dummy")
    public List<Course> getDummies() {
        Course dummy1 = new Course();
        dummy1.setId(UUID.randomUUID().toString());
        dummy1.setName("Diklat PBJ");

        Course dummy2 =
                Course
                        .builder()
                        .name("Diklat Java")
                        .shortDescription("Lorem")
                        .build();

        List<Course> courseList = new ArrayList<>();
        courseList.add(dummy1);
        courseList.add(dummy2);

        return courseList;
    }
}
