package id.go.kemenkeu.bppk.lmsku.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Slf4j
public class IgnoreProps {
    public IgnoreProps() {
        log.error("Error! Trying to create IgnoreProps instances.");
        throw new AssertionError("No instances of IgnoreProps created!");
    }

    public static String[] invoke(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        emptyNames.add("id");
        emptyNames.add("createdAt");
        emptyNames.add("createdBy");
        emptyNames.add("updatedAt");
        emptyNames.add("updatedBy");

        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }

        String[] result = new String[emptyNames.size()];

        return emptyNames.toArray(result);
    }

    public static String[] addPropsToIgnore(String[] source, String... propsToIgnore) {
        Set<String> sourceList = new HashSet<>(Arrays.asList(source));
        Set<String> addition = new HashSet<>(Arrays.asList(propsToIgnore));
        sourceList.addAll(addition);

        String[] finalIgnored = new String[sourceList.size()];
        sourceList.toArray(finalIgnored);
        return finalIgnored;
    }
}
