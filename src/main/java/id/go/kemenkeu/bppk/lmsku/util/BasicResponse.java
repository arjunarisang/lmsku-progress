package id.go.kemenkeu.bppk.lmsku.util;

import lombok.Data;

@Data
public class BasicResponse {
    private String message;

    public BasicResponse(String message) {
        this.message = message;
    }
}
