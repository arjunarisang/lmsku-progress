package id.go.kemenkeu.bppk.lmsku.entity;

import id.go.kemenkeu.bppk.lmsku.entity.audit.AuditModel;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@EntityListeners({AuditingEntityListener.class})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Section extends AuditModel implements Serializable {

    private static final long serialVersionUID = 6368656307770409958L;

    @Id
    @GeneratedValue(generator = "uid")
    @GenericGenerator(name = "uid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    private String name;
}
