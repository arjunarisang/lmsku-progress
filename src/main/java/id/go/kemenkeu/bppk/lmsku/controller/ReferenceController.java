package id.go.kemenkeu.bppk.lmsku.controller;

import id.go.kemenkeu.bppk.lmsku.enums.CourseType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/reference")
public class ReferenceController {

    @GetMapping("/courseType")
    public List<String> getCourseType() {
        List<String> types = new ArrayList<>();
        for (CourseType t : CourseType.values()) {
            types.add(t.getCourseType());
        }
        return types;
    }

}
