package id.go.kemenkeu.bppk.lmsku.entity;

import id.go.kemenkeu.bppk.lmsku.entity.audit.AuditModel;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@EntityListeners({AuditingEntityListener.class})
public class Filez extends AuditModel implements Serializable {

    private static final long serialVersionUID = -7197851069514050046L;

    @Id
    @GeneratedValue(generator = "uid")
    @GenericGenerator(name = "uid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String bucket;

    @Column(nullable = false)
    private String object;

    @Column(nullable = false)
    private String groupUid;

    @Column
    private Boolean isOriginal;

    @Column(nullable = false)
    private String contentType;

    @Column
    private Long height;
}
