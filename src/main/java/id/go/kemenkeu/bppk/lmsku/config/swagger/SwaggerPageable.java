package id.go.kemenkeu.bppk.lmsku.config.swagger;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

// package config.swagger
@Data
public class SwaggerPageable implements Serializable {

    private static final long serialVersionUID = 1590924480428981705L;

    @ApiModelProperty(value = "Results page you want to retrieve (0..N)", example = "0")
    private int page;

    @ApiModelProperty(value = "Number of records per page", example = "20")
    private int size;

    @ApiModelProperty(
            value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.",
            example = "&sort=created,asc"
    )
    private Sort sort;
}
