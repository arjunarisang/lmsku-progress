package id.go.kemenkeu.bppk.lmsku.entity;

import id.go.kemenkeu.bppk.lmsku.entity.audit.AuditModel;
import id.go.kemenkeu.bppk.lmsku.enums.CourseType;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@EntityListeners({AuditingEntityListener.class})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Course extends AuditModel implements Serializable {

    private static final long serialVersionUID = 5741531216620359146L;

    @Id
    @GeneratedValue(generator = "uid")
    @GenericGenerator(name = "uid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    @NotNull
    @NotBlank
    @Column(nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CourseType courseType;

    @Column(nullable = false)
    private String slug;

    private String shortDescription;

    @NotNull
    private LocalDateTime startDate;

    @NotNull
    private LocalDateTime endDate;
}
